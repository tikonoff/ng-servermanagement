import {Component, Input} from '@angular/core';
import {Server} from '../model/server.model';
import {AlerterComponent} from '../components/alerter/alerter.component';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  providers: [ Server, AlerterComponent ]
})

export class ServersComponent{
  name: string;
  servers = [
    new Server(),
    new Server(),
    new Server(),
  ];

  constructor (private alert: AlerterComponent) {}

  onCreateServer() {
    this.servers.push(new Server(this.name));
    this.alert.showSuccess(
      'Server №' + this.servers.length + ', "' + this.servers[(this.servers.length)-1].name + '" – added!');
    this.name = '';
  }

  RemoveServer(server: Server) {
    let index = this.servers.indexOf(server);
    this.alert.showError(
      'Server №' + (index + 1) + ', "' + this.servers[index].name + '" – removed!');
    this.servers.splice(index, 1);
  }

}

