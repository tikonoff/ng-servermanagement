import {Component, Injectable, ViewContainerRef} from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-alerter',
  templateUrl: 'alerter.component.html',
})

@Injectable()

export class AlerterComponent {
  hidden = true;
  msg: string;

  constructor(public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  Show (title: string) {
    this.msg = title;
    this.hidden = false;
    setTimeout(() => {
      this.hidden = true;
    }, 3000);
  }


  showSuccess(title: string) {
    this.toastr.success(title);
  }

  showError(title: string) {
    this.toastr.error(title);
  }

  showWarning(title: string) {
    this.toastr.warning(title);
  }

  showInfo(title: string) {
    this.toastr.info(title);
  }

}
