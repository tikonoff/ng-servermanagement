import {Component, EventEmitter, Injectable, Input, Output} from '@angular/core';
import {Server} from '../model/server.model';
import {AlerterComponent} from "../components/alerter/alerter.component";


@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styles: [`
    a{cursor: pointer;}
  `],
})


export class ServerComponent {
  @Input() server: Server
  @Output() removeServer = new EventEmitter();

  constructor (private alert: AlerterComponent) {}

  ChangeStatus () {
    let msg: string
    this.server.status = (!this.server.status)
    msg = (this.server.status) ? 'Server ' + this.server.name + ' is up' : 'Server ' + this.server.name + ' is down'
    if (this.server.status)
      this.alert.showInfo(msg);
    else
      this.alert.showWarning(msg);
  }

  RemoveIt() {
    this.removeServer.emit(this.server);
  }

}
