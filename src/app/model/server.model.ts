import {Injectable} from '@angular/core';

@Injectable()
export class Server {
  id: string;
  name: string;
  status = false;

  constructor (name?: string) {
    // create random if no name supplied
    this.name = (name && name.match(/\S/)) ? name : Math.random().toString(36).substring(7).toUpperCase();
    this.id = (new Date()).toString();
  }

  ToggleStatus () {
    this.status = (this.status) ? false : true;
  }


}
