import {Component, OnInit } from '@angular/core';
import { AlerterComponent } from './components/alerter/alerter.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ AlerterComponent ]
})

export class AppComponent implements OnInit {

  constructor(public alert: AlerterComponent) {
  }

  ngOnInit() {
    this.alert.showInfo('Service is ready');
  }


}
